# input() reads a string with a line of input, stripping the ' ' (newline) at the end.
# This is all you need for most Kick Start problems.
t = int(input()) # read a line with a single integer
for t_i in range(1, t + 1):
  n, b = [int(s) for s in input().split(" ")] # read a list of integers, 2 in this case
  a = [int(s) for s in input().split(" ")] # read a list of integers
  res = 0
  for price in sorted(a):
    b -= price
    res += 1
    if b < 0:
      res -= 1
      break
  print("Case #{}: {}".format(t_i, res))
  # check out .format's specification for more formatting options
