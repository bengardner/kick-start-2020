import heapq
t = int(input())
for t_i in range(1, t + 1):
  n, k = [int(s) for s in input().split(" ")]
  m = [int(s) for s in input().split(" ")]
  d = []
  for i in range(1, len(m)):
    d.append(-(m[i] - m[i - 1]))
  heapq.heapify(d)
  for i in range(k):
    top = d[0]
    heapq.heapreplace(d, top // 2)
    heapq.heappush(d, (top + 1) // 2)
  print("Case #{}: {}".format(t_i, -d[0]))
  # check out .format's specification for more formatting options
