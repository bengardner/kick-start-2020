# Boilerplate

This is to be used as a starting point for submissions.

## Steps before submitting

- Copy this directory
- Rename .py file
- Edit .py file as needed for the question