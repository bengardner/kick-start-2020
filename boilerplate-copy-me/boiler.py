# input() reads a string with a line of input, stripping the ' ' (newline) at the end.
# This is all you need for most Kick Start problems.
t = int(input()) # read a line with a single integer
for t_i in range(1, t + 1):
  "Inputs"
  n = int(input()) # read an integer
  a = [int(i) for i in input().split(" ")] # read a list of integers

  "Work"
  all_ints = [] # example intermediary structure
  for i in range(len(a)):
    all_ints.append(a[i])

  "Output"
  res = len(all_ints)
  print("Case #{}: {}".format(t_i, res))
  # check out .format's specification for more formatting options
