from collections import deque

# input() reads a string with a line of input, stripping the ' ' (newline) at the end.
# This is all you need for most Kick Start problems.
t = int(input()) # read a line with a single integer
for t_i in range(1, t + 1):
  program = input()

  # Order of translations does not affect the result, so let's worryn't about it
  w, h = (0, 0)
  m = deque([1])
  i = 0
  while i < len(program):
    if program[i] == 'N':
      h -= m[-1]
    elif program[i] == 'S':
      h += m[-1]
    elif program[i] == 'E':
      w += m[-1]
    elif program[i] == 'W':
      w -= m[-1]
    elif program[i] == ')':
      m.pop()
    else:
      m.append(int(program[i]) * m[-1])
      i += 1
    i += 1

  print("Case #{}: {} {}".format(t_i, w % 10**9 + 1, h % 10**9 + 1))
  # check out .format's specification for more formatting options
