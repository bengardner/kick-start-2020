# input() reads a string with a line of input, stripping the ' ' (newline) at the end.
# This is all you need for most Kick Start problems.
t = int(input()) # read a line with a single integer
for t_i in range(1, t + 1):
  n, d = [int(s) for s in input().split(" ")]
  x = [int(s) for s in input().split(" ")] # read a list of integers

  nextDeadline = d
  for i in range(len(x) - 1, -1, -1):
    nextDeadline -= nextDeadline % x[i]
  
  print("Case #{}: {}".format(t_i, nextDeadline))
  # check out .format's specification for more formatting options
