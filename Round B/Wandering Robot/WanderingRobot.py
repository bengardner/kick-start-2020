# Exceeds memory limit

# input() reads a string with a line of input, stripping the ' ' (newline) at the end.
# This is all you need for most Kick Start problems.
t = int(input()) # read a line with a single integer
for t_i in range(1, t + 1):
  w, h, l, u, r, d = [int(s) for s in input().split(" ")]

  # I don't like the labelling scheme
  l -= 1
  u -= 1
  r -= 1
  d -= 1

  p = [[0] * w for _ in range(h)]
  p[0][0] = 1

  # Construct row and column 1
  for col in range(1, w):
    if 0 < u or col < l or col > r:
      p[0][col] = p[0][col - 1] / 2
  for row in range(1, h):
    if 0 < l or row < u or row > d:
      p[row][0] = p[row - 1][0] / 2
      
  for row in range(1, h):
    for col in range(1, w):
      if col < l or col > r or row < u or row > d:
        if row == h - 1:
          p[row][col] += p[row][col - 1] / 2
        if col == w - 1:
          p[row][col] += p[row - 1][col] / 2
        p[row][col] += p[row - 1][col] / 2 + p[row][col - 1] / 2
  
  print("Case #{}: {}".format(t_i, p[-1][-1]))
  # check out .format's specification for more formatting options
