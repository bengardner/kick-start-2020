# Exceeds time limit

# input() reads a string with a line of input, stripping the ' ' (newline) at the end.
# This is all you need for most Kick Start problems.
t = int(input()) # read a line with a single integer
for t_i in range(1, t + 1):
  w, h, l, u, r, d = [int(s) for s in input().split(" ")]

  # I don't like the labelling scheme
  l -= 1
  u -= 1
  r -= 1
  d -= 1

  curCol = [1] + [0] * (h - 1)
  curRow = [1] + [0] * (w - 1)

  # Construct row and column 1
  for col in range(1, w):
    if 0 < u or col < l or col > r:
      curRow[col] = curRow[col - 1] / 2
  for row in range(1, h):
    if 0 < l or row < u or row > d:
      curCol[row] = curCol[row - 1] / 2
      
  for row in range(1, h):
    for col in range(1, w):
      if col < l or col > r or row < u or row > d:
        if col == w - 1:  # At the right
          curRow[col] = curRow[col] + curCol[row] / 2
          if row == h - 1:  # At the bottom, too
            curRow[col] += curCol[row] / 2
        elif row == h - 1:  # At the bottom
          curRow[col] = curRow[col] / 2 + curCol[row]
        else:
          curRow[col] = curRow[col] / 2 + curCol[row] / 2
        curCol[row] = curRow[col]
      else:
        curRow[col] = 0
        curCol[row] = 0
    if row < u or row > d:
      curRow[0] /= 2
    else:
      curRow[0] = 0
        
  print("Case #{}: {}".format(t_i, curRow[-1]))
  # check out .format's specification for more formatting options
