# input() reads a string with a line of input, stripping the ' ' (newline) at the end.
# This is all you need for most Kick Start problems.
t = int(input()) # read a line with a single integer
for t_i in range(1, t + 1):
  n = int(input())
  h = [int(s) for s in input().split(" ")] # read a list of integers
  res = []
  for i in range(1, len(h) - 1):
    if h[i] > h[i-1] and h[i] > h[i+1]:
      res.append(h[i])
  print("Case #{}: {}".format(t_i, len(res)))
  # check out .format's specification for more formatting options
