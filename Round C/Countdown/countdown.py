# input() reads a string with a line of input, stripping the ' ' (newline) at the end.
# This is all you need for most Kick Start problems.
t = int(input()) # read a line with a single integer
for t_i in range(1, t + 1):
  "Inputs"
  n, k = [int(i) for i in input().split(" ")]
  a = [int(i) for i in input().split(" ")]

  "Work"
  counter = k
  res = 0
  for i in range(len(a)):
    if a[i] == counter:
      counter -= 1
      if counter == 0:
        counter = k
        res += 1
    else:
      counter = k - 1 if a[i] == k else k

  "Output"
  print("Case #{}: {}".format(t_i, res))
  # check out .format's specification for more formatting options
