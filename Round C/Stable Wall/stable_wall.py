# input() reads a string with a line of input, stripping the ' ' (newline) at the end.
# This is all you need for most Kick Start problems.
t = int(input()) # read a line with a single integer
for t_i in range(1, t + 1):
  "Inputs"
  r, c = [int(i) for i in input().split(" ")]
  a = []
  for r_i in range(1, r + 1):
    a.append(input())

  "Work"
  """
  Algorithm:
  
  - Gather all viable candidates for next polyomino by searching the bottom row,
  defined as the bottommost sequence of supported squares. These don't
  necessarily need to be in a row. Record these polyomino types in a list.
  
  - One by one, recursively test viable candidates using the fulfilment
  criteria, until the topmost row is supported, or there are no more
  polyominoes to test.
  """
  def get_candidates(supports):
    pass

  def test_candidates():
    pass
  
  "Output"
  print("Case #{}: {}".format(t_i, res))
  # check out .format's specification for more formatting options
